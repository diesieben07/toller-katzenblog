{extends file="layout.tpl"}
{block name="content"}
    {foreach $katzen as $katze}
        <figure class="figure">
            <a href="index.php?page=katze&katze={$katze['id']}">
                <img height="300" src="index.php?page=img&id={$katze['id']}">
            </a>
            <figcaption>
                {$katze['name']|escape:'html'}
            </figcaption>
        </figure>
    {/foreach}

    <p>
        <a href="index.php?page=new">Neue Katze</a>
    </p>
{/block}