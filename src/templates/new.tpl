<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Toller Katzenblog</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    <link href="/style.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h1>Neue Katze</h1>
    <form action="/index.php?page=new" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label for="title" class="col-sm-2 col-form-label">Titel</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" required placeholder="Titel" name="title" id="title">
            </div>
        </div>
        <div class="form-group row">
            <label for="image" class="col-sm-2 col-form-label">Bild</label>
            <div class="col-sm-5">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" required name="image" id="image">
                    <label for="image" class="custom-file-label">Bild</label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="text" class="col-sm-2 col-form-label">Text</label>
            <div class="col-sm-5">
                <textarea class="form-control" required name="text" id="text"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <button type="submit" class="btn">Speichern</button>
        </div>
    </form>
</div>
</body>
</html>