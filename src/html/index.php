<?php

ini_set('display_errors', 1);

require __DIR__ . '/../vendor/autoload.php';

$db = new PDO('mysql:host=localhost;dbname=katzen', 'local', 'local');

$page = $_GET['page'] ?? 'index';

switch (mb_strtolower($page)) {
    case 'katze':
        katze();
        break;
    case 'new':
        newForm();
        break;
    case 'delete':
        delete();
        break;
    case 'comment':
        comment();
        break;
    case "img":
        img();
        break;
    default:
        home();
}

function newSmarty(): Smarty {
    $smarty = new Smarty();
    $smarty->setTemplateDir(__DIR__ . '/../templates');
    $smarty->setCacheDir(__DIR__ . '/../.smarty/cache');
    $smarty->setCompileDir(__DIR__ . '/../.smarty/compile');
    return $smarty;
}

function home($info = null) {
    $cats = allCats();

    $smarty = newSmarty();
    $smarty->assign('katzen', $cats);
    $smarty->assign('alerts', $info);
    $smarty->display('index.tpl');
}

function img() {
    global $db;
    $stmt = $db->prepare("SELECT image from cats where id = ${_GET['id']}");
    if (!$stmt->execute()) {
        http_response_code(500);
        echo "sadface";
    } else {
        header("content-type: image/jpeg");
        $stmt->bindColumn(1, $handle, PDO::PARAM_LOB);
        $stmt->fetch(PDO::FETCH_BOUND);
        echo $handle;
    }
}

function delete() {
    global $db;
    $db->exec("DELETE FROM cats WHERE id = ${_POST['id']}");
    home([['success', 'Gelöscht!']]);
}

function comment() {
    global $db;
    $katze = $_GET['katze'];
    if ($db->exec("INSERT INTO comment (name, text, cat_id) VALUES ('${_POST['name']}', '${_POST['text']}', $katze)") === false) {
        katze([['error', 'Kommentar konnte nicht gespeichert werden.' . json_encode($db->errorInfo())]]);
    } else {
        katze([['success', 'Kommentar gespeichert!']]);
    }
}

function newForm() {
    global $db;
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $fh = fopen($_FILES['image']['tmp_name'], 'rb');
        try {
            $stmt = $db->prepare('INSERT INTO cats (name, description, image) VALUES (?, ?, ?)');
            $stmt->bindValue(1, $_POST['title']);
            $stmt->bindValue(2, $_POST['text']);
            $stmt->bindValue(3, $fh, PDO::PARAM_LOB);
            if ($stmt->execute()) {
                home([['success', 'Gespeichert!']]);
            } else {
                home([['error', 'Konnte nicht gespeichert werden: ' . json_encode($db->errorInfo())]]);
            }
        } finally {
            fclose($fh);
        }
    } else {
        $smarty = newSmarty();
        $smarty->display('new.tpl');
    }
}

function katze($info = null) {
    global $db;
    $katzeId = $_GET['katze'];
    $stmt = $db->query("SELECT * FROM cats WHERE id=$katzeId", PDO::FETCH_ASSOC);
    if ($stmt === false || ($katze = $stmt->fetch()) === false) {
        http_response_code(404);
        var_dump($db->errorInfo());
        echo "not found :(";
    } else {
        $comments = $db->query("SELECT * FROM `comment` WHERE `cat_id`=${katzeId}", PDO::FETCH_ASSOC)->fetchAll();

        $smarty = newSmarty();
        $smarty->assign('katze', $katze);
        $smarty->assign('comments', $comments);
        $smarty->assign('alerts', $info);
        $smarty->display('katze.tpl');
    }
}

function allCats(): array {
    global $db;
    $stmt = $db->query('SELECT id, name, description, image FROM cats');
    if ($stmt === false) {
        echo json_encode($db->errorInfo());
        echo "\n" . json_encode(get_current_user());
        exit;
    }
    return $stmt->fetchAll();
}