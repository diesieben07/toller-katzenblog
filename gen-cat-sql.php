<?php

// generates SQL migrations for all images in /cats folder

$cats = scandir(__DIR__ . '/cats');

$outFolder = __DIR__ . '/sql/';

$mysql = mysqli_init();

foreach ($cats as $cat) {
    if ($cat === '.' || $cat === '..') {
        continue;
    }

    $file = __DIR__ . '/cats/' . $cat;

    $hex = bin2hex(file_get_contents($file));

    $text = "INSERT INTO `cats` (`name`, `description`, `image`) VALUES ('$cat', '', UNHEX('$hex'));";

    $sqlFile = "${outFolder}R__$cat.sql";
    file_put_contents($sqlFile, $text);
}