{extends file="layout.tpl"}
{block name="content"}
    <h2>{$katze['name']|escape}</h2>
    <p><a href="/">&lt; Zurück</a></p>
    <img height="300" src="index.php?page=img&id={$katze['id']}">
    <p>
        {$katze['description']|escape}
    </p>
    <form action="/index.php?page=delete" method="post">
        <input type="hidden" name="id" value="{$katze['id']}">
        <div class="form-group row">
            <button type="submit" class="btn btn-link">Löschen</button>
        </div>
    </form>

    <h3>Kommentare</h3>
    {if $comments|count > 0}
        <dl>
            {foreach $comments as $comment}
                <dt>{$comment['name']}</dt>
                <dd>{$comment['text']}</dd>
            {/foreach}
        </dl>
    {else}
        <p>Noch keine Kommentare.</p>
    {/if}


    <form action="/index.php?page=comment&katze={$katze['id']}" method="post">
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Ihr Name</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" required placeholder="Ihr Name" name="name" id="name">
            </div>
        </div>
        <div class="form-group row">
            <label for="text" class="col-sm-2 col-form-label">Ihr Kommentar</label>
            <div class="col-sm-5">
                <textarea class="form-control" required name="text" id="text"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <button type="submit" class="btn">Speichern</button>
        </div>
    </form>
{/block}