create table cats
(
	id int auto_increment primary key,
	name varchar(255) not null,
	description text not null,
	image LONGBLOB null
);