create table katzen.comment
(
  id int auto_increment
    primary key,
  name varchar(255) not null,
  text text not null,
  cat_id int null,
  constraint comment_cats_id_fk
  foreign key (cat_id) references katzen.cats (id)
    on update cascade on delete cascade
)
;